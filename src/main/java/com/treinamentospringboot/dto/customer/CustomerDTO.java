package com.treinamentospringboot.dto.customer;

import java.io.Serializable;

/**
 * Classe representativa json customer
 * 
 * @author tiago.canatelli
 *
 */
public class CustomerDTO implements Serializable {

  /**
   * ID
   */
  private static final long serialVersionUID = -3986978743660464482L;

  /** id customer */
  private java.lang.String id;
  /** id crm */
  private java.lang.String crmId;
  /** base URL */
  private java.lang.String baseUrl;
  /** name */
  private java.lang.String name;
  /** login */
  private java.lang.String login;

  /**
   * Construtor da Representação Customer
   * 
   * @param id
   * @param crmId
   * @param baseUrl
   * @param name
   * @param login
   */
  public CustomerDTO(String id, String crmId, String baseUrl, String name, String login) {
    super();
    this.id = id;
    this.crmId = crmId;
    this.baseUrl = baseUrl;
    this.name = name;
    this.login = login;
  }

  /**
   * Construtor Padrão
   */
  public CustomerDTO() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * Construtor do Customer Convert Entity to Representation
   * 
   * @param id
   * @param crmId
   * @param baseUrl
   * @param name
   * @param login
   */
  public CustomerDTO(com.treinamentospringboot.model.customer.Customer customer) {
    super();
    this.id = customer.getId();
    this.crmId = customer.getCrmId();
    this.baseUrl = customer.getBaseUrl();
    this.name = customer.getName();
    this.login = customer.getLogin();
  }

  /**
   * @return the id
   */
  public java.lang.String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(java.lang.String id) {
    this.id = id;
  }

  /**
   * @return the crmId
   */
  public java.lang.String getCrmId() {
    return crmId;
  }

  /**
   * @param crmId the crmId to set
   */
  public void setCrmId(java.lang.String crmId) {
    this.crmId = crmId;
  }

  /**
   * @return the baseUrl
   */
  public java.lang.String getBaseUrl() {
    return baseUrl;
  }

  /**
   * @param baseUrl the baseUrl to set
   */
  public void setBaseUrl(java.lang.String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * @return the name
   */
  public java.lang.String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(java.lang.String name) {
    this.name = name;
  }

  /**
   * @return the login
   */
  public java.lang.String getLogin() {
    return login;
  }

  /**
   * @param login the login to set
   */
  public void setLogin(java.lang.String login) {
    this.login = login;
  }
}
