package com.treinamentospringboot.dto;

/**
 * Interface métodos comuns nas classes de representação (json)
 * 
 * @author tiago.canatelli
 *
 */
public interface IRepresentation {

}
