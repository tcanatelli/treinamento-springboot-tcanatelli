package com.treinamentospringboot.repository.customer;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.treinamentospringboot.dto.customer.CustomerDTO;
import com.treinamentospringboot.model.customer.Customer;

/**
 * Repositorio para o customer
 * 
 * @author tiago.canatelli
 *
 */
public interface ICustomerRepository extends JpaRepository<Customer, String> {

  /**
   * Pesquisa pelo login
   * 
   * @param login
   * @return
   */
  List<Customer> findByLogin(String login);

  /**
   * Lista todos customers convertidos em DTO
   * 
   * @return
   */
  @Query("select new com.treinamentospringboot.dto.customer.CustomerDTO(c.id, c.crmId, c.baseUrl, c.name, c.login) "
      + "from Customer c")
  List<CustomerDTO> findAllCustomer();


}
