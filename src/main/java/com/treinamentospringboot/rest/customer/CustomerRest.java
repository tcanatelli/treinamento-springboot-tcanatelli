package com.treinamentospringboot.rest.customer;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.treinamentospringboot.dto.customer.CustomerDTO;
import com.treinamentospringboot.infrastructure.rest.IRest;
import com.treinamentospringboot.service.customer.ICustomerService;

@RestController
@RequestMapping("/api")
public class CustomerRest implements IRest {

  /** Customer service */
  @Autowired
  private ICustomerService customerService;

  /**
   * Lista todos os customers
   * 
   * @return
   */
  @GetMapping("/customers")
  public List<CustomerDTO> getAllNotes() {
    return this.customerService.list();
  }

  /**
   * Salvar novo customer
   * 
   * @param customer
   * @return
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  @PostMapping("/customers")
  public ResponseEntity<CustomerDTO> save(@Valid @RequestBody CustomerDTO customer) {
    try {
      CustomerDTO response = this.customerService.save(customer);
      return new ResponseEntity<CustomerDTO>(response, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity(e.getMessage(), HttpStatus.CONFLICT);
    }
  }

  /**
   * Retorna customer pelo id
   * 
   * @param id
   * @return
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  @GetMapping("/customers/{id}")
  public ResponseEntity<CustomerDTO> getNoteById(@PathVariable(value = "id") String id) {
    CustomerDTO customer = this.customerService.getById(id);
    if (customer == null) {
      return new ResponseEntity("Customer with this ID not found", HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok().body(customer);
  }

  /**
   * Retorna customer pelo id
   * 
   * @param id
   * @return
   */
  @DeleteMapping("/customers/{id}")
  public ResponseEntity<CustomerDTO> delete(@PathVariable(value = "id") String id) {
    try {
      this.customerService.delete(id);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }

  /**
   * Atualiza customer total
   * 
   * @param id
   * @param customer
   * @return
   */
  @PutMapping("/customers/{id}")
  public ResponseEntity<CustomerDTO> update(@PathVariable(value = "id") String id,
      @Valid @RequestBody CustomerDTO customer) {
    customer.setId(id);
    customer = this.customerService.update(customer);
    if (customer == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(customer);
  }

  /**
   * Patch Customer
   * 
   * @param id
   * @param customer
   * @return
   */
  @PatchMapping("/customers/{id}")
  public ResponseEntity<CustomerDTO> patch(@PathVariable(value = "id") String id,
      @Valid @RequestBody CustomerDTO customer) {
    customer.setId(id);
    customer = this.customerService.patch(customer);
    if (customer == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(customer);
  }

}
