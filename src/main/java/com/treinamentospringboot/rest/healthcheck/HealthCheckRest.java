package com.treinamentospringboot.rest.healthcheck;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.treinamentospringboot.infrastructure.rest.IRest;

@RestController
@RequestMapping("/api/healthcheck")
public class HealthCheckRest implements IRest {

  /**
   * Get health check.
   * 
   * @return
   */
  @GetMapping
  public ResponseEntity<String> list() {
    return new ResponseEntity<String>("{'status': 'OK'}", HttpStatus.OK);
  }

}
