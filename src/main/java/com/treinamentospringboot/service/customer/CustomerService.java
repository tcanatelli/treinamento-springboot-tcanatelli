package com.treinamentospringboot.service.customer;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.treinamentospringboot.dto.customer.CustomerDTO;
import com.treinamentospringboot.model.customer.Customer;
import com.treinamentospringboot.repository.customer.ICustomerRepository;

/**
 * Classe implementa os métodos dominio service customer
 * 
 * @author tiago.canatelli
 *
 */
@Service
public class CustomerService implements ICustomerService {

  @Autowired
  private ICustomerRepository customerRepository;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.treinamentospringboot.service.customer.ICustomerService#save(com.treinamentospringboot.
   * representation.customer.Customer)
   */
  @Override
  public CustomerDTO save(CustomerDTO customer) throws Exception {

    try {
      // Verifica se existe customer com o mesmo login
      List<Customer> exists = this.customerRepository.findByLogin(customer.getLogin());

      if (exists != null && !exists.isEmpty()) {
        throw new Exception("An existing item(login) already exists");
      }

      // Cria entidade
      com.treinamentospringboot.model.customer.Customer customerEntity =
          new com.treinamentospringboot.model.customer.Customer(customer);

      // Salva novo customer
      customerEntity = customerRepository.save(customerEntity);
      // Cria customer retorno
      customer = new CustomerDTO(customerEntity);
      return customer;
    } catch (Exception e) {
      throw e;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.treinamentospringboot.service.customer.ICustomerService#list()
   */
  @Override
  public List<CustomerDTO> list() {
    return this.customerRepository.findAllCustomer();
  }

  @Override
  public CustomerDTO getById(String id) {
    Customer customer = customerRepository.findOne(id);
    if (customer == null) {
      return null;
    }
    return new CustomerDTO(customer);
  }

  @Override
  public CustomerDTO update(CustomerDTO customer) {
    Customer customerEntity = customerRepository.findOne(customer.getId());
    if (customerEntity == null) {
      return null;
    }
    customerEntity = new Customer(customer);

    Customer updatedCustomer = customerRepository.save(customerEntity);
    return new CustomerDTO(updatedCustomer);
  }

  @Override
  public void delete(String id) throws Exception {
    Customer customer = customerRepository.findOne(id);
    if (customer == null) {
      throw new Exception();
    }

    customerRepository.delete(customer);
  }

  @Override
  public CustomerDTO patch(CustomerDTO customer) {
    Customer customerEntity = customerRepository.findOne(customer.getId());
    if (customerEntity == null) {
      return null;
    }
    customerEntity = new Customer();
    customerEntity.setId(customer.getId());
    customerEntity.setBaseUrl(customer.getBaseUrl());
    customerEntity.setCrmId(customer.getCrmId());
    customerEntity.setLogin(customer.getLogin());
    customerEntity.setName(customer.getName());

    Customer updatedCustomer = customerRepository.save(customerEntity);
    return new CustomerDTO(updatedCustomer);
  }


}
