package com.treinamentospringboot.service.customer;

import java.util.List;
import com.treinamentospringboot.dto.customer.CustomerDTO;
import com.treinamentospringboot.infrastructure.service.IService;

/**
 * Interface com os metodos do dominio service customer
 * 
 * @author tiago.canatelli
 *
 */
public interface ICustomerService extends IService {

  /**
   * Salva um novo customer
   * 
   * @param customer
   * @return
   * @throws Exception
   */
  CustomerDTO save(CustomerDTO customer) throws Exception;

  /**
   * Lista todos customers
   * 
   * @return
   */
  List<CustomerDTO> list();

  /**
   * Pesquisa pelo ID
   * 
   * @param id
   * @return
   */
  CustomerDTO getById(String id);

  /**
   * Substitiu objeto
   * 
   * @param customer
   * @return
   */
  CustomerDTO update(CustomerDTO customer);

  /**
   * Excluir customer
   * 
   * @param id
   * @throws Exception
   */
  void delete(String id) throws Exception;

  /**
   * Patch customer
   * 
   * @param customer
   * @return
   */
  CustomerDTO patch(CustomerDTO customer);

}
