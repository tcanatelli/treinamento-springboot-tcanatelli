package com.treinamentospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Classe de Inicialização Spring Boot
 * 
 * @author tiago.canatelli
 *
 */
@SpringBootApplication
@EnableJpaAuditing
public class TreinamentoSpringbootApplication {

  /**
   * Metodo principal
   * 
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(TreinamentoSpringbootApplication.class, args);
  }
}
