package com.treinamentospringboot.async.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import com.treinamentospringboot.async.executor.ProcessoAssincronoAsync;

/**
 * Bean criado ao qual pode ser executar alguma tarefa assíncrona na iniciaização.
 * 
 * @author tiago.canatelli
 *
 */
@Configuration
@EnableAsync
public class SpringAsyncConfig {

  /**
   * Tarefa Assincrona que pode ser executada.
   * 
   * @return
   */
  @Bean
  public ProcessoAssincronoAsync executarTarefa() {
    return new ProcessoAssincronoAsync();
  }

}
