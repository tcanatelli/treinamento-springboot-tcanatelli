package com.treinamentospringboot.async.executor;

import org.springframework.scheduling.annotation.Async;

/**
 * Classe assíncrona que implementa a tarefa à ser executada.
 * 
 * @author tiago.canatelli
 *
 */
public class ProcessoAssincronoAsync {

  /**
   * Implementação da task ou chamada à subtasks que podem ser executados.
   */
  @Async
  public void runTask() {
    System.out.printf("Execução tarefa exemplo de thread: %s%n", Thread.currentThread().getName());
  }
}
