package com.treinamentospringboot.model.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entidade Customer
 * 
 * @author tiago.canatelli
 *
 */
@Entity
@Table(name = "Customer")
@EntityListeners(AuditingEntityListener.class)
public class Customer {

  /** ID */
  @Id
  @Column(name = "id")
  private java.lang.String id;
  /** CRMID */
  private java.lang.String crmId;
  /** BASEURL */
  private java.lang.String baseUrl;
  /** NAME */
  private java.lang.String name;
  /** LOGIN */
  private java.lang.String login;

  /**
   * Construtor do Customer
   * 
   * @param id
   * @param crmId
   * @param baseUrl
   * @param name
   * @param login
   */
  public Customer(String id, String crmId, String baseUrl, String name, String login) {
    super();
    this.id = id;
    this.crmId = crmId;
    this.baseUrl = baseUrl;
    this.name = name;
    this.login = login;
  }

  /**
   * Construtor do Customer Convert Representation to Entity
   * 
   * @param id
   * @param crmId
   * @param baseUrl
   * @param name
   * @param login
   */
  public Customer(com.treinamentospringboot.dto.customer.CustomerDTO customerRepresentation) {
    super();
    this.id = customerRepresentation.getId();
    this.crmId = customerRepresentation.getCrmId();
    this.baseUrl = customerRepresentation.getBaseUrl();
    this.name = customerRepresentation.getName();
    this.login = customerRepresentation.getLogin();
  }

  /**
   * Construtor Padrão
   */
  public Customer() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @return the id
   */
  public java.lang.String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(java.lang.String id) {
    this.id = id;
  }

  /**
   * @return the crmId
   */
  public java.lang.String getCrmId() {
    return crmId;
  }

  /**
   * @param crmId the crmId to set
   */
  public void setCrmId(java.lang.String crmId) {
    this.crmId = crmId;
  }

  /**
   * @return the baseUrl
   */
  public java.lang.String getBaseUrl() {
    return baseUrl;
  }

  /**
   * @param baseUrl the baseUrl to set
   */
  public void setBaseUrl(java.lang.String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * @return the name
   */
  public java.lang.String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(java.lang.String name) {
    this.name = name;
  }

  /**
   * @return the login
   */
  public java.lang.String getLogin() {
    return login;
  }

  /**
   * @param login the login to set
   */
  public void setLogin(java.lang.String login) {
    this.login = login;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Customer other = (Customer) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }


}
